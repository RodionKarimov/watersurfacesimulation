//#pragma OPENCL EXTENSION cl_khr_fp16 : enable



//===Defines=============================================
//#define I_U_SHORT_MAX 65535

//#define INFINITY_WEIGHT                 32760.0f

// #define LEFT -10.f
// #define BOTTOM -10.f
// #define WIDTH 20.f
// #define HEIGHT 20.f
// #define NEAR -10.f
// #define FAR 10.f

// #define I_MAX_SUBNODES_IN_NODE 2
// #define I_MAX_STORAGE_SPHERES_IN_NODE 100


//===Constants===========================================
//const float                             INFINITY_WEIGHT                         =  32760.0f;



//===Types.==============================================
// struct TRay
// {
//   // Origin
//   //float ox, oy, oz, ow;
//   float4 v4O;
//   // Direction
//   //float dx, dy, dz, dw;
//   float4 v4D;
//   // Intersection distance
//   float maxt;
// };

// struct TRaysPacket
// {
//   // Origin
//   float4 v4O_X;
//   float4 v4O_Y;
//   float4 v4O_Z;

//   // Direction
//   float4 v4D_X;
//   float4 v4D_Y;
//   float4 v4D_Z;

//   // Intersection distance
//   float4 v4MaxT;

// };

// struct TResultColors
// {
//   float4 f4R;
//   float4 f4G;
//   float4 f4B;
//   float4 f4A;

// };

// struct sphere
// {
//   // Center
//   float cx, cy, cz, cw;
//   //float4 v4Center;
//   // Shpere color
//   float r, g, b/*, a*/;
//   //float4 v4Color;
//   // Radius
//   //float radius;

//   bool m_bInFrustum;
// };



// struct TBVHNode {
//   float3 f3BottomLeft;
//   float3 f3TopRight;

//   ushort aSubNodesIndices [ I_MAX_SUBNODES_IN_NODE ];
//   uchar uchar_SubNodesAmount;
//   ushort aSpheresIndices [ I_MAX_STORAGE_SPHERES_IN_NODE ];
//   uchar uchar_SpheresAmount;

// };



//===Functions.==========================================
// bool solve_quadratic ( float a, float b, float c, float * _pX1, float * _pX2 )
// {
//   float d = b*b - 4 * a*c;

//   if (d < 0)
//     return false;
//   else
//   {
//     float den = 1 / (2 * a);
//     * _pX1 = (-b - sqrt(d))*den;
//     * _pX2 = (-b + sqrt(d))*den;
//     return true;
//   }

// }

// bool intersect_sphere ( __global const struct sphere * sphere, struct TRay * r )
// {
//   float4 v4RO = r -> v4O - ( float4 ) ( sphere -> cx, sphere -> cy, sphere -> cz, sphere -> cw );

//   float a = //rtemp.dx * rtemp.dx + rtemp.dy * rtemp.dy + rtemp.dz * rtemp.dz;
//     dot ( r -> v4D, r -> v4D );
//   float b = //2 * (rtemp.ox * rtemp.dx + rtemp.oy * rtemp.dy + rtemp.oz * rtemp.dz);
//     2.0f * dot ( v4RO, r -> v4D );
//   float c = //rtemp.ox * rtemp.ox + rtemp.oy * rtemp.oy + rtemp.oz * rtemp.oz - sphere.radius * sphere.radius;
//     dot ( v4RO, v4RO ) - sphere -> cw * sphere -> cw;

//   float t0, t1;

//   if ( solve_quadratic ( a, b, c, & t0, & t1 ) )
//   {
//     if (t0 > r -> maxt || t1 < 0.f)
//       return false;

//     r -> maxt = t0 > 0.f ? t0 : t1;
//     return true;
//   }

//   return false;

// }



//===Main functions======================================
__kernel void OpenCL_WaterSurfaceSimulation ( //__global const float * _AdjacencyMatrix,
                               //__global float * _ShortestPathsWeightsMatrix,
                               //__global int * _NextElementsMatrix,
                               //const int _k,
		               //const int _AmountOfNodes
                      // __global const struct sphere * _aSpheres,     // __global const struct sphere * _aSpheres
                      // __global float * _aImageResult,
                      __global float * _aVertices,
                      // __global float * _aNormals,
                      // __global float * _aTangents,
                      const uint _iAmountOfVerticesOnXAxis,
                      const uint _iAmountOfVerticesOnYAxis,
                      const float _fDistanceBetweenVerticesOnXAxis,
                      const float _fDistanceBetweenVerticesOnYAxis,
                      // const uint _i_kNumSpheres
                      const float _fTime
                    ) {
    const int                             idx                 = get_global_id ( 0 );
    uint                                  j                   = idx / _iAmountOfVerticesOnXAxis;
    uint                                  i                   = idx % _iAmountOfVerticesOnYAxis;

    //struct TRay                           structureRay;



    /*for ( j = 0; j < _AmountOfNodes; j ++ ) {
      /
        First we check that paths from i to k and from k to j exist - they both are bigger than 0. Then we check - whether sum of weights of
       travelling on them is less, than weight of travelling on current shortest path from i to j. If it is so - we set new shortest path and
       weight of travelling on it.
      /
      if ( ( _ShortestPathsWeightsMatrix [ idx * _AmountOfNodes + _k ] < INFINITY_WEIGHT ) && ( _ShortestPathsWeightsMatrix [ _k * _AmountOfNodes + j ] < INFINITY_WEIGHT ) &&
           ( _ShortestPathsWeightsMatrix [ idx * _AmountOfNodes + j ] > ( _ShortestPathsWeightsMatrix [ idx * _AmountOfNodes + _k ] + _ShortestPathsWeightsMatrix [ _k * _AmountOfNodes + j ] ) )
         ) {
        _ShortestPathsWeightsMatrix [ idx * _AmountOfNodes + j ]                  = _ShortestPathsWeightsMatrix [ idx * _AmountOfNodes + _k ] + _ShortestPathsWeightsMatrix [ _k * _AmountOfNodes + j ];

        _NextElementsMatrix [ idx * _AmountOfNodes + j ]                          = _NextElementsMatrix [ _k * _AmountOfNodes + j ];
        
      } //-if

      //_ShortestPathsWeightsMatrix [ idx * _AmountOfNodes + j ]                    = _ShortestPathsWeightsMatrix [ idx * _AmountOfNodes + j ] + 1;
      //_ShortestPathsWeightsMatrix [ idx * _AmountOfNodes + j ]                    = j;

    } //-for*/



    // structureRay.v4O.x               = LEFT + ( WIDTH / _iImageWidth ) * ( i + 0.5f );
    // structureRay.v4O.y               = BOTTOM + ( HEIGHT / _iImageHeight ) * ( j + 0.5f );
    // structureRay.v4O.z               = NEAR;
    // structureRay.v4O.w               = 0.0f;

    // structureRay.v4D.x               = 0.0f;
    // structureRay.v4D.y               = 0.0f;
    // structureRay.v4D.z               = 1.0f;
    // structureRay.v4D.w               = 0.0f;

    // structureRay.maxt                = FAR - NEAR;



    // int iSphereId = -1;

    // for (int k = 0U; k < _i_kNumSpheres; ++k)
    // {
    //   if ( _aSpheres[k].m_bInFrustum && intersect_sphere ( & _aSpheres[k], & structureRay ) )
    //   {
    //     iSphereId = k;
    //   }
    // }



    // //_aImageResult [ idx * 3 ]            = 0.1f;
    // //_aImageResult [ idx * 3 + 1 ]        = 0.4f;
    // //_aImageResult [ idx * 3 + 2 ]        = 0.8f;

    // //_aImageResult [ idx * 3 ]            = _aSpheres [ 1 ].r;
    // //_aImageResult [ idx * 3 + 1 ]        = _aSpheres [ 1 ].g;
    // //_aImageResult [ idx * 3 + 2 ]        = _aSpheres [ 1 ].b;

    // _aImageResult [ idx * 3 ]            = iSphereId > 0 ? _aSpheres [ iSphereId ].r : 0.1f;
    // _aImageResult [ idx * 3 + 1 ]        = iSphereId > 0 ? _aSpheres [ iSphereId ].g : 0.1f;
    // _aImageResult [ idx * 3 + 2 ]        = iSphereId > 0 ? _aSpheres [ iSphereId ].b : 0.1f;

    _aVertices [ idx * 3 ]            = i * _fDistanceBetweenVerticesOnXAxis;
    _aVertices [ idx * 3 + 1 ]        = j * _fDistanceBetweenVerticesOnYAxis;
    _aVertices [ idx * 3 + 2 ]        = 20.0f * sin ( convert_float ( i ) / _iAmountOfVerticesOnXAxis * 20.0f + _fTime ) * sin ( convert_float ( j ) / _iAmountOfVerticesOnYAxis * 20.0f + _fTime );
  
}

__kernel void OpenCL_ComputeTangentsAndNormals ( 
                      __global float * _aVertices,
                      __global float * _aNormals,
                      __global float * _aTangents,
                      const uint _iAmountOfVerticesOnXAxis,
                      const uint _iAmountOfVerticesOnYAxis
                    ) {
    const int                             idx                 = get_global_id ( 0 );
    uint                                  j                   = idx / _iAmountOfVerticesOnXAxis;
    uint                                  i                   = idx % _iAmountOfVerticesOnYAxis;
    uint                                  iNextI              = i + 1 < _iAmountOfVerticesOnXAxis ? i + 1 : i - 1;
    uint                                  iNextJ              = j + 1 < _iAmountOfVerticesOnYAxis ? j + 1 : j - 1;

    float3 f3Tangent;
    float3 f3BiTangent;
    float3 f3Normal;

    float3 f3VertexA = ( float3 ) {
      _aVertices [ 3 * ( ( j ) * _iAmountOfVerticesOnXAxis + i ) ],
      _aVertices [ 3 * ( ( j ) * _iAmountOfVerticesOnXAxis + i ) + 1 ],
      _aVertices [ 3 * ( ( j ) * _iAmountOfVerticesOnXAxis + i ) + 2 ]
      };
    float3 f3VertexB = ( float3 ) {
      _aVertices [ 3 * ( ( j ) * _iAmountOfVerticesOnXAxis + iNextI ) ],
      _aVertices [ 3 * ( ( j ) * _iAmountOfVerticesOnXAxis + iNextI ) + 1 ],
      _aVertices [ 3 * ( ( j ) * _iAmountOfVerticesOnXAxis + iNextI ) + 2 ]
      };

    f3Tangent = normalize ( f3VertexB - f3VertexA );

    f3VertexA = ( float3 ) {
      _aVertices [ 3 * ( ( j ) * _iAmountOfVerticesOnXAxis + i ) ],
      _aVertices [ 3 * ( ( j ) * _iAmountOfVerticesOnXAxis + i ) + 1 ],
      _aVertices [ 3 * ( ( j ) * _iAmountOfVerticesOnXAxis + i ) + 2 ]
      };
    f3VertexB = ( float3 ) {
      _aVertices [ 3 * ( ( iNextJ ) * _iAmountOfVerticesOnXAxis + i ) ],
      _aVertices [ 3 * ( ( iNextJ ) * _iAmountOfVerticesOnXAxis + i ) + 1 ],
      _aVertices [ 3 * ( ( iNextJ ) * _iAmountOfVerticesOnXAxis + i ) + 2 ]
      };

    f3BiTangent = normalize ( f3VertexB - f3VertexA );

    f3Normal = cross ( f3Tangent, f3BiTangent );
    f3Normal = ( i + 1 < _iAmountOfVerticesOnXAxis ) && ( j + 1 < _iAmountOfVerticesOnYAxis ) ||
               ( i + 1 == _iAmountOfVerticesOnXAxis ) && ( j + 1 == _iAmountOfVerticesOnYAxis )
               ? f3Normal : - f3Normal;

    _aTangents [ idx * 3 ]           = f3Tangent.x;
    _aTangents [ idx * 3 + 1 ]       = f3Tangent.y;
    _aTangents [ idx * 3 + 2 ]       = f3Tangent.z;

    _aNormals [ idx * 3 ]            = f3Normal.x;
    _aNormals [ idx * 3 + 1 ]        = f3Normal.y;
    _aNormals [ idx * 3 + 2 ]        = f3Normal.z;
  
}
