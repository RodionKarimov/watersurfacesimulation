// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#pragma comment ( lib, "OpenCL.lib" )

#include <iostream>
#include <chrono>
#include <cstdlib>
#include <cstdint>

#include "fstream"
#include "string"

#include <vector>

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"

//#pragma OPENCL EXTENSION cl_khr_fp16 : enable
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
//#include "ThirdParty/OpenCL/inc/CL/cl.h"
#include "CL/cl.h"
//#undef CL_VERSION_1_0
//#undef CL_VERSION_1_1
#undef CL_VERSION_2_0

#include "WaterPlane.generated.h"



const cl_uint I_AMOUNT_OF_VERTICES_ON_X_AXIS = 100;
const cl_float F_DISTANCE_BETWEEN_VERTICES_ON_X_AXIS = 10.0f;
const cl_uint I_AMOUNT_OF_VERTICES_ON_Y_AXIS = 100;
const cl_float F_DISTANCE_BETWEEN_VERTICES_ON_Y_AXIS = 10.0f;

const cl_uint                           AMOUNT_OF_PLATFORM_IDS                      =  10;
const cl_uint                           SIZE_OF_STRING_FOR_INFORMATION_GETTING      =  256;

const cl_uint                           MAX_OPENCL_PROGRAM_BUILD_LOG                =  10000;
const size_t                            NUMBER_OF_WORKITEMS_PER_WORK_GROUP          =  512;



UCLASS()
class WATERSIMULATION_API AWaterPlane : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWaterPlane();

	virtual bool ShouldTickIfViewportsOnly () const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void BeginDestroy() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY ( VisibleAnywhere )
	UProceduralMeshComponent * m_pMesh;

	void PostActorCreated ();
	void PostLoad ();

	void CreateTriangle ();
	void UpdateWaterPlane ( float _fTime );

	void CreateMesh ( float _fTime, TArray < FVector > & _aVertices, TArray < FVector > & _aNormals, TArray < FProcMeshTangent > & _aTangents,
		TArray < FVector2D > & _aTextureCoordinates, TArray < FLinearColor > & _aVerticesColors, TArray < int32 > & _aTriangles );



	//---OpenCL.----------------------------------------------------------
	cl_platform_id                          m_aPlatforms [ 10 ];
	cl_uint                                 m_iAmountOfAvailablePlatforms;

	cl_device_id                            m_CLDeviceId;
	cl_context                              m_CLContext;
	cl_command_queue                        m_CLCommandQueue;

	cl_program                              m_CLProgram;
	char                                    m_aOCLProgramSource [ 100000 ];
	const char *                            m_pOCLProgramSource;

	char                                    m_aOCLProgramBuildLog [ 100000 ];

	cl_kernel                               m_CLKernel;
	// cl_kernel                               m_CLKernel_4RaysPacketTracing;
	// cl_kernel                               m_CLKernel_4RaysPacketTracing_WithBVHTree;
	cl_kernel                               m_CLKernel_ComputeTangentsAndNormals;

	// Coordinates, Normals and Tangents in one structure. -
	// Coordinates.
	cl_mem                                  cl_mem_Vertices;
	cl_mem                                  cl_mem_Normals;
	cl_mem                                  cl_mem_Tangents;
	// cl_mem                                  cl_mem_BiTangents;

	char                                   m_sInformation [ 256 ];
	size_t                                 m_cl_uint_SizeOfReturnedData;

	std :: vector < float >                m_aVertices;
	std :: vector < float >                m_aNormals;
	std :: vector < float >                m_aTangents;



	void InitializeOpenCL ();
	void CloseOpenCL ();
	void OpenCL_InitResources ();
	void OpenCL_FreeResources ();
	void OpenCL_PerformCalculations ();

};
