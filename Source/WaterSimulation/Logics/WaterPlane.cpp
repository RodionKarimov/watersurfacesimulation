// Fill out your copyright notice in the Description page of Project Settings.

#include "WaterPlane.h"



// Sets default values
AWaterPlane::AWaterPlane()
{
	m_pMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("ProceduralMeshComponent"));
	RootComponent = m_pMesh;
    // New in UE 4.17, multi-threaded PhysX cooking.
    m_pMesh->bUseAsyncCooking = true;

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

bool AWaterPlane :: ShouldTickIfViewportsOnly () const {
	return true;
}

// Called when the game starts or when spawned
void AWaterPlane::BeginPlay()
{
	Super::BeginPlay();
	
}

void AWaterPlane::BeginDestroy() {
	OpenCL_FreeResources ();
	CloseOpenCL ();

	Super::BeginDestroy();

}

// Called every frame
void AWaterPlane::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateWaterPlane ( GetWorld () -> GetTimeSeconds () );

}

// This is called when actor is spawned (at runtime or when you drop it into the world in editor)
void AWaterPlane::PostActorCreated()
{
	InitializeOpenCL ();
	OpenCL_InitResources ();

	Super::PostActorCreated();

	CreateTriangle();
}

// This is called when actor is already in level and map is opened
void AWaterPlane::PostLoad()
{
	InitializeOpenCL ();
	OpenCL_InitResources ();

	Super::PostLoad();

	CreateTriangle();
}

void AWaterPlane::CreateTriangle()
{
	TArray<FVector> vertices;
	// vertices.Add(FVector(0, 0, 0));
	// vertices.Add(FVector(0, 100, 0));
	// vertices.Add(FVector(100, 0, 0));

	TArray<int32> Triangles;
	// Triangles.Add(0);
	// Triangles.Add(1);
	// Triangles.Add(2);

	TArray<FVector> normals;
	// normals.Add(FVector(0, 0, 1));
	// normals.Add(FVector(0, 0, 1));
	// normals.Add(FVector(0, 0, 1));

	TArray<FVector2D> UV0;
	// UV0.Add(FVector2D(0, 0));
	// UV0.Add(FVector2D(10, 0));
	// UV0.Add(FVector2D(0, 10));
	

	TArray<FProcMeshTangent> tangents;
	// tangents.Add(FProcMeshTangent(0, 1, 0));
	// tangents.Add(FProcMeshTangent(0, 1, 0));
	// tangents.Add(FProcMeshTangent(0, 1, 0));

	TArray<FLinearColor> vertexColors;
	// vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	// vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	// vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));

	CreateMesh ( 0.0f, vertices, normals, tangents, UV0, vertexColors, Triangles );

	m_pMesh->CreateMeshSection_LinearColor(0, vertices, Triangles, normals, UV0, vertexColors, tangents, true);
        
    // Enable collision data
	m_pMesh->ContainsPhysicsTriMeshData(true);
}



void AWaterPlane :: UpdateWaterPlane ( float _fTime ) {
	TArray<FVector> vertices;
	TArray<int32> Triangles;
	TArray<FVector> normals;
	TArray<FVector2D> UV0;
	TArray<FProcMeshTangent> tangents;
	TArray<FLinearColor> vertexColors;

	CreateMesh ( _fTime, vertices, normals, tangents, UV0, vertexColors, Triangles );

	m_pMesh->UpdateMeshSection_LinearColor(0, vertices, normals, UV0, vertexColors, tangents);
}



void AWaterPlane :: CreateMesh ( float _fTime, TArray < FVector > & _aVertices, TArray < FVector > & _aNormals, TArray < FProcMeshTangent > & _aTangents,
	TArray < FVector2D > & _aTextureCoordinates, TArray < FLinearColor > & _aVerticesColors, TArray < int32 > & _aTriangles ) {

	FVector vTangent;
	FProcMeshTangent ProcMeshTangent_ArrayElement;
	FVector vBiTangent;
	TArray < FVector > aBiTangents;
	FVector vNormal;

	int i;
	int j;

	// for ( j = 0; j < I_AMOUNT_OF_VERTICES_ON_Y_AXIS; j ++ ) {
	// 	for ( i = 0; i < I_AMOUNT_OF_VERTICES_ON_X_AXIS; i ++ ) {
	// 		_aVertices.Add ( FVector ( i * F_DISTANCE_BETWEEN_VERTICES_ON_X_AXIS, j * F_DISTANCE_BETWEEN_VERTICES_ON_Y_AXIS,
	// 			20.0f * FMath :: Sin ( float ( i ) / I_AMOUNT_OF_VERTICES_ON_X_AXIS * 20.0f + _fTime ) *
	// 			  FMath :: Sin ( float ( j ) / I_AMOUNT_OF_VERTICES_ON_Y_AXIS * 20.0f + _fTime ) ) );
	// 		// _aNormals.Add ( FVector(0, 0, 1) );
	// 		_aTextureCoordinates.Add(FVector2D(0, 0));
	// 		// _aTangents.Add(FProcMeshTangent(0, 1, 0));
	// 		_aVerticesColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	// 	} //-for
	// } //-for

	OpenCL_PerformCalculations ();
	for ( j = 0; j < I_AMOUNT_OF_VERTICES_ON_Y_AXIS; j ++ ) {
		for ( i = 0; i < I_AMOUNT_OF_VERTICES_ON_X_AXIS; i ++ ) {
			_aVertices.Add ( FVector (
				m_aVertices [ 3 * ( ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ) ],
				m_aVertices [ 3 * ( ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ) + 1 ],
				m_aVertices [ 3 * ( ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ) + 2 ]
				) );

			_aTextureCoordinates.Add(FVector2D(0, 0));
			_aVerticesColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
		} //-for
	} //-for



	//---Calculating tangents.---------------------------------------
	// for ( j = 0; j < I_AMOUNT_OF_VERTICES_ON_Y_AXIS - 1; j ++ ) {
	// 	for ( i = 0; i < I_AMOUNT_OF_VERTICES_ON_X_AXIS - 1; i ++ ) {
	// 		vTangent = _aVertices [ ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i + 1 ] - _aVertices [ ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ];
	// 		vTangent.Normalize ();
	// 		_aTangents.Add ( FProcMeshTangent ( vTangent, false ) );
	// 		vBiTangent = _aVertices [ ( j + 1 ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ] - _aVertices [ ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ];
	// 		vBiTangent.Normalize ();
	// 		aBiTangents.Add ( vBiTangent );
	// 	} //-for
	// 	_aTangents.Add ( FProcMeshTangent ( vTangent, false ) );
	// 	aBiTangents.Add ( vBiTangent );
	// } //-for

	// j = I_AMOUNT_OF_VERTICES_ON_Y_AXIS - 1;
	// for ( i = 0; i < I_AMOUNT_OF_VERTICES_ON_X_AXIS; i ++ ) {
	// 	ProcMeshTangent_ArrayElement = _aTangents [ ( j - 1 ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ];
	// 	_aTangents.Add ( ProcMeshTangent_ArrayElement );
	// 	vBiTangent = aBiTangents [ ( j - 1 ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ];
	// 	aBiTangents.Add ( vBiTangent );
	// } //-for

	for ( j = 0; j < I_AMOUNT_OF_VERTICES_ON_Y_AXIS; j ++ ) {
		for ( i = 0; i < I_AMOUNT_OF_VERTICES_ON_X_AXIS; i ++ ) {
			_aTangents.Add ( FProcMeshTangent (
				m_aTangents [ 3 * ( ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ) ],
				m_aTangents [ 3 * ( ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ) + 1 ],
				m_aTangents [ 3 * ( ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ) + 2 ]
				) );

		} //-for
	} //-for

	//---Calculating normals.----------------------------------------
	for ( j = 0; j < I_AMOUNT_OF_VERTICES_ON_Y_AXIS; j ++ ) {
		for ( i = 0; i < I_AMOUNT_OF_VERTICES_ON_X_AXIS; i ++ ) {
			// vNormal = FVector :: CrossProduct ( _aTangents [ ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ].TangentX, aBiTangents [ ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ] );
			// _aNormals.Add ( vNormal );
			
			_aNormals.Add ( FVector (
				m_aNormals [ 3 * ( ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ) ],
				m_aNormals [ 3 * ( ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ) + 1 ],
				m_aNormals [ 3 * ( ( j ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i ) + 2 ]
				) );

		} //-for
	} //-for



	for ( j = 0; j < I_AMOUNT_OF_VERTICES_ON_Y_AXIS - 1; j ++ ) {
		for ( i = 0; i < I_AMOUNT_OF_VERTICES_ON_X_AXIS - 1; i ++ ) {
			// First triangle.
			_aTriangles.Add ( j * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i );
			_aTriangles.Add ( ( j + 1 ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i );
			_aTriangles.Add ( j * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i + 1 );

			// Second triangle.
			_aTriangles.Add ( ( j + 1 ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i );
			_aTriangles.Add ( ( j + 1 ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i + 1 );
			_aTriangles.Add ( j * I_AMOUNT_OF_VERTICES_ON_X_AXIS + i + 1 );

		} //-for
	} //-for
}



//---OpenCL.----------------------------------------------------------
void AWaterPlane :: InitializeOpenCL () {
	cl_int                                  iR;
	cl_uint i = 0;

	iR                                     = clGetPlatformIDs ( AMOUNT_OF_PLATFORM_IDS, m_aPlatforms, & m_iAmountOfAvailablePlatforms );
	if ( iR != CL_SUCCESS ) {
		//ShowMessage ( "Application", "Error in getting platform ID." );
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in getting platform ID." ), * GetName () );
	}

	// itoa ( g_iAmountOfAvailablePlatforms, StringForNumbersShowing, 10 );
	// ShowMessage ( "Application", "Amount of available platform IDs", StringForNumbersShowing );
	UE_LOG ( LogTemp, Log, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Amount of available platform IDs : %i ." ), * GetName (), m_iAmountOfAvailablePlatforms );

	for ( i = 0; i < m_iAmountOfAvailablePlatforms; i ++ ) {
		iR                                   = clGetPlatformInfo ( m_aPlatforms [ i ], CL_PLATFORM_PROFILE, SIZE_OF_STRING_FOR_INFORMATION_GETTING, m_sInformation, & m_cl_uint_SizeOfReturnedData );
		if ( iR != CL_SUCCESS )
			UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in getting CL_PLATFORM_PROFILE platform info." ), * GetName () );

		UE_LOG ( LogTemp, Log, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : CL_PLATFORM_PROFILE : %s ." ), * GetName (), * FString ( m_sInformation ) );

		iR                                   = clGetPlatformInfo ( m_aPlatforms [ i ], CL_PLATFORM_VERSION, SIZE_OF_STRING_FOR_INFORMATION_GETTING, m_sInformation, & m_cl_uint_SizeOfReturnedData );
		if ( iR != CL_SUCCESS )
			UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in getting CL_PLATFORM_VERSION platform info." ), * GetName () );

		UE_LOG ( LogTemp, Log, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : CL_PLATFORM_VERSION : %s ." ), * GetName (), * FString ( m_sInformation ) );

		iR                                   = clGetPlatformInfo ( m_aPlatforms [ i ], CL_PLATFORM_NAME, SIZE_OF_STRING_FOR_INFORMATION_GETTING, m_sInformation, & m_cl_uint_SizeOfReturnedData );
		if ( iR != CL_SUCCESS )
			UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in getting CL_PLATFORM_NAME platform info." ), * GetName () );

		UE_LOG ( LogTemp, Log, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : CL_PLATFORM_NAME : %s ." ), * GetName (), * FString ( m_sInformation ) );

		iR                                   = clGetPlatformInfo ( m_aPlatforms [ i ], CL_PLATFORM_VENDOR, SIZE_OF_STRING_FOR_INFORMATION_GETTING, m_sInformation, & m_cl_uint_SizeOfReturnedData );
		if ( iR != CL_SUCCESS )
			UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in getting CL_PLATFORM_VENDOR platform info." ), * GetName () );

		UE_LOG ( LogTemp, Log, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : CL_PLATFORM_VENDOR : %s ." ), * GetName (), * FString ( m_sInformation ) );

		iR                                   = clGetPlatformInfo ( m_aPlatforms [ i ], CL_PLATFORM_EXTENSIONS, SIZE_OF_STRING_FOR_INFORMATION_GETTING, m_sInformation, & m_cl_uint_SizeOfReturnedData );
		if ( iR != CL_SUCCESS )
			UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in getting CL_PLATFORM_EXTENSIONS platform info." ), * GetName () );

		UE_LOG ( LogTemp, Log, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : CL_PLATFORM_EXTENSIONS : %s ." ), * GetName (), * FString ( m_sInformation ) );

	} //-for

	cl_uint    cl_uint_NumDevices = 1;
	i = 0;
	iR = -1;
	//g_CLDeviceId = -1;
	while ( ( i < m_iAmountOfAvailablePlatforms ) && ( iR != CL_SUCCESS ) ) {
		iR                                     = clGetDeviceIDs ( m_aPlatforms [ i ], CL_DEVICE_TYPE_GPU, 1, & m_CLDeviceId, & cl_uint_NumDevices );
		if ( iR != CL_SUCCESS ) {
			UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in getting device IDs : %i, %i." ), * GetName (), i, m_aPlatforms [ i ] );

		} else {
			UE_LOG ( LogTemp, Log, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Device ID has successfully been gotten : %i, %i." ), * GetName (), i, m_aPlatforms [ i ] );

		} //-else

		i = i + 1;

	} //-while

	if ( m_CLDeviceId ) {
		m_CLContext                              = clCreateContext ( NULL, 1, & m_CLDeviceId, NULL, NULL, & iR );
		if ( iR != CL_SUCCESS )
			UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in context creation." ), * GetName () );

		m_CLCommandQueue                         = clCreateCommandQueue ( m_CLContext, m_CLDeviceId, NULL, & iR );
		if ( iR != CL_SUCCESS )
			UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in queue creation." ), * GetName () );

	} //-if
}

void AWaterPlane :: CloseOpenCL () {
	if ( m_CLCommandQueue )
		clReleaseCommandQueue ( m_CLCommandQueue );

	if ( m_CLContext )
  		clReleaseContext ( m_CLContext );
}

void AWaterPlane :: OpenCL_InitResources () {
	cl_int                                  iR;

	FString sPath = FPaths :: GameDir () + "Binaries/Data/Common/Kernels/rt.cu";

	if ( ! m_CLDeviceId )
		return;



	std :: string                         sContentsOfKernelSourceFile;
	size_t                                size_t_SizeOfKernelSourceFile;



	UE_LOG ( LogTemp, Log, TEXT ( "AWaterPlane.OpenCL_InitResources () : %s : sPath : %s ." ), * GetName (), * sPath );

	m_aVertices.resize ( I_AMOUNT_OF_VERTICES_ON_X_AXIS * I_AMOUNT_OF_VERTICES_ON_X_AXIS * 3 );
	m_aNormals.resize ( I_AMOUNT_OF_VERTICES_ON_X_AXIS * I_AMOUNT_OF_VERTICES_ON_X_AXIS * 3 );
	m_aTangents.resize ( I_AMOUNT_OF_VERTICES_ON_X_AXIS * I_AMOUNT_OF_VERTICES_ON_X_AXIS * 3 );



	/*cl_mem_AdjacencyMatrix                = clCreateBuffer ( Context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof ( float ) * AMOUNT_OF_NODES * AMOUNT_OF_NODES, AdjacencyMatrix, & R );
	if ( R != CL_SUCCESS ) ShowMessage ( "Application", "Error in creation of buffer for adjacency matrix data." );

	cl_mem_ShortestPathsWeightsMatrix     = clCreateBuffer ( Context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof ( float ) * AMOUNT_OF_NODES * AMOUNT_OF_NODES, ShortestPathsWeights, & R );
	if ( R != CL_SUCCESS ) ShowMessage ( "Application", "Error in creation of buffer for ShortestPathsWeightsMatrix data." );

	cl_mem_NextElementsMatrix             = clCreateBuffer ( Context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof ( int ) * AMOUNT_OF_NODES * AMOUNT_OF_NODES, MatrixOfNextElements, & R );
	if ( R != CL_SUCCESS ) ShowMessage ( "Application", "Error in creation of buffer for MatrixOfNextElements data." );*/

	// cl_mem_Spheres = clCreateBuffer ( g_CLContext, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof ( sphere ) * kNumSpheres, spheres, & g_iR );
	// if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in creation of buffer for spheres data." );

	// cl_mem_BVHTree = clCreateBuffer ( g_CLContext, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof ( TBVHNode ) * g_std_vector_BVHNodesList.size (), & g_std_vector_BVHNodesList [ 0 ], & g_iR );
	// if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in creation of buffer for cl_mem_BVHTree." );

	cl_mem_Vertices             = clCreateBuffer ( m_CLContext, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof ( float ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS * I_AMOUNT_OF_VERTICES_ON_Y_AXIS * 3, & m_aVertices [ 0 ], & iR );
	if ( iR != CL_SUCCESS ) {
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in creation of buffer for m_aVertices data." ), * GetName () );
	}

	cl_mem_Normals             = clCreateBuffer ( m_CLContext, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof ( float ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS * I_AMOUNT_OF_VERTICES_ON_Y_AXIS * 3, & m_aNormals [ 0 ], & iR );
	if ( iR != CL_SUCCESS ) {
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in creation of buffer for m_aNormals data." ), * GetName () );
	}

	cl_mem_Tangents             = clCreateBuffer ( m_CLContext, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof ( float ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS * I_AMOUNT_OF_VERTICES_ON_Y_AXIS * 3, & m_aTangents [ 0 ], & iR );
	if ( iR != CL_SUCCESS ) {
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in creation of buffer for m_aTangents data." ), * GetName () );
	}



	//---Reading kernel------------------------------------
	std :: ifstream                       KernelSourceFile ( * sPath, std :: ios :: in | std :: ios :: binary );

	if ( ! KernelSourceFile.is_open () ) {
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in openning of kernel source file." ), * GetName () );
	} else {
		KernelSourceFile.seekg ( 0, std :: ios :: end );
		sContentsOfKernelSourceFile.resize ( KernelSourceFile.tellg () );
		size_t_SizeOfKernelSourceFile              = KernelSourceFile.tellg ();
		KernelSourceFile.seekg ( 0, std :: ios :: beg );
		KernelSourceFile.read ( & sContentsOfKernelSourceFile [ 0 ], sContentsOfKernelSourceFile.size () );
		KernelSourceFile.close ();

		//ShowMessage ( "Application", "Kernel source file contents : ", PCHAR ( ContentsOfKernelSourceFile.c_str () ) );

	} //-else

	strcpy ( m_aOCLProgramSource, sContentsOfKernelSourceFile.c_str () );
	m_pOCLProgramSource                     = m_aOCLProgramSource;
	m_CLProgram                           = clCreateProgramWithSource ( m_CLContext, 1, & m_pOCLProgramSource, & size_t_SizeOfKernelSourceFile, & iR );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in creation of program from source file." ), * GetName () );

	iR                                     = clBuildProgram ( m_CLProgram, 1, & m_CLDeviceId, NULL, NULL, NULL );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in building of program." ), * GetName () );

	clGetProgramBuildInfo ( m_CLProgram, m_CLDeviceId, CL_PROGRAM_BUILD_LOG, MAX_OPENCL_PROGRAM_BUILD_LOG, m_aOCLProgramBuildLog, NULL );
	UE_LOG ( LogTemp, Log, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : OpenCL program build log : %s ." ), * GetName (), * FString ( m_aOCLProgramBuildLog ) );

	m_CLKernel               = clCreateKernel ( m_CLProgram, "OpenCL_WaterSurfaceSimulation", & iR );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in creation of kernel OpenCL_WaterSurfaceSimulation." ), * GetName () );
	m_CLKernel_ComputeTangentsAndNormals = clCreateKernel ( m_CLProgram, "OpenCL_ComputeTangentsAndNormals", & iR );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in creation of kernel OpenCL_ComputeTangentsAndNormals." ), * GetName () );

}

void AWaterPlane :: OpenCL_FreeResources () {
	if ( ! m_CLDeviceId )
		return;

	if ( m_CLKernel )
		clReleaseKernel ( m_CLKernel );
	if ( m_CLKernel_ComputeTangentsAndNormals )
		clReleaseKernel ( m_CLKernel_ComputeTangentsAndNormals );
	// clReleaseKernel ( g_CLKernel_4RaysPacketTracing );
	// clReleaseKernel ( g_CLKernel_4RaysPacketTracing_WithBVHTree );

	//clReleaseMemObject ( cl_mem_AdjacencyMatrix );
	//clReleaseMemObject ( cl_mem_ShortestPathsWeightsMatrix );
	//clReleaseMemObject ( cl_mem_AdjacencyMatrix );
	// clReleaseMemObject ( cl_mem_Spheres );
	// clReleaseMemObject ( cl_mem_BVHTree );
	if ( cl_mem_Vertices )
		clReleaseMemObject ( cl_mem_Vertices );
	if ( cl_mem_Normals )
		clReleaseMemObject ( cl_mem_Normals );
	if ( cl_mem_Tangents )
		clReleaseMemObject ( cl_mem_Tangents );

}

void AWaterPlane :: OpenCL_PerformCalculations () {
	cl_int                                  iR;



	if ( ! m_CLDeviceId )
		return;

	/*R                                     = clSetKernelArg ( cl_kernel_FloydWarshall, 0, sizeof ( cl_mem ), & cl_mem_AdjacencyMatrix );
	if ( R != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument cl_mem_AdjacencyMatrix." );
	R                                     = clSetKernelArg ( cl_kernel_FloydWarshall, 1, sizeof ( cl_mem ), & cl_mem_ShortestPathsWeightsMatrix );
	if ( R != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument cl_mem_AdjacencyMatrix." );
	R                                     = clSetKernelArg ( cl_kernel_FloydWarshall, 2, sizeof ( cl_mem ), & cl_mem_NextElementsMatrix );
	if ( R != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument cl_mem_AdjacencyMatrix." );
	R                                     = clSetKernelArg ( cl_kernel_FloydWarshall, 4, sizeof ( size_t ), & AMOUNT_OF_NODES );
	if ( R != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument AMOUNT_OF_NODES." );*/



	//---Single ray raytracing.----------------------------------------
	// iR                                     = clSetKernelArg ( m_CLKernel, 0, sizeof ( cl_mem ), & cl_mem_Spheres );
	// if ( iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument cl_mem_Spheres." );

	iR                                     = clSetKernelArg ( m_CLKernel, 0, sizeof ( cl_mem ), & cl_mem_Vertices );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in setting of kernel argument cl_mem_Vertices." ), * GetName () );

	iR                                     = clSetKernelArg ( m_CLKernel, 1, sizeof ( cl_uint ), & I_AMOUNT_OF_VERTICES_ON_X_AXIS );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in setting of kernel argument I_AMOUNT_OF_VERTICES_ON_X_AXIS." ), * GetName () );
	iR                                     = clSetKernelArg ( m_CLKernel, 2, sizeof ( cl_uint ), & I_AMOUNT_OF_VERTICES_ON_Y_AXIS );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in setting of kernel argument I_AMOUNT_OF_VERTICES_ON_Y_AXIS." ), * GetName () );
	iR                                     = clSetKernelArg ( m_CLKernel, 3, sizeof ( cl_float ), & F_DISTANCE_BETWEEN_VERTICES_ON_X_AXIS );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in setting of kernel argument F_DISTANCE_BETWEEN_VERTICES_ON_X_AXIS." ), * GetName () );
	iR                                     = clSetKernelArg ( m_CLKernel, 4, sizeof ( cl_float ), & F_DISTANCE_BETWEEN_VERTICES_ON_Y_AXIS );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in setting of kernel argument F_DISTANCE_BETWEEN_VERTICES_ON_Y_AXIS." ), * GetName () );
	cl_float fTime = GetWorld () -> GetTimeSeconds ();;
	iR                                     = clSetKernelArg ( m_CLKernel, 5, sizeof ( cl_float ), & fTime );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in setting of kernel argument fTime." ), * GetName () );

	// //g_iR                                     = clSetKernelArg ( g_CLKernel, 1, sizeof ( size_t ), & kNumSpheres );
	// //if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument kNumSpheres." );
	// iR                                     = clSetKernelArg ( m_CLKernel, 4, sizeof ( cl_uint ), & kNumSpheres );
	// if ( iR != CL_SUCCESS ) {
	// 	ShowMessage ( "Application", "Error in setting of kernel argument kNumSpheres." );
	// 	std::cout << "iR " << iR << " .\n";

	// }

	size_t     size_tGlobalWorkSize = I_AMOUNT_OF_VERTICES_ON_X_AXIS * I_AMOUNT_OF_VERTICES_ON_Y_AXIS;
	iR                                   = clEnqueueNDRangeKernel ( m_CLCommandQueue, m_CLKernel, 1, NULL, & size_tGlobalWorkSize, NULL, 0, NULL, NULL );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in kernel execution." ), * GetName () );



	/*//---4-rays packet raytracing.-------------------------------------
	g_iR                                     = clSetKernelArg ( g_CLKernel_4RaysPacketTracing, 0, sizeof ( cl_mem ), & cl_mem_Spheres );
	if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument cl_mem_Spheres." );

	g_iR                                     = clSetKernelArg ( g_CLKernel_4RaysPacketTracing, 1, sizeof ( cl_mem ), & cl_mem_ImageResult );
	if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument cl_mem_ImageResult." );

	g_iR                                     = clSetKernelArg ( g_CLKernel_4RaysPacketTracing, 2, sizeof ( cl_uint ), & kImageWidth );
	if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument kImageWidth." );
	g_iR                                     = clSetKernelArg ( g_CLKernel_4RaysPacketTracing, 3, sizeof ( cl_uint ), & kImageHeight );
	if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument kImageHeight." );

	g_iR                                     = clSetKernelArg ( g_CLKernel_4RaysPacketTracing, 4, sizeof ( cl_uint ), & kNumSpheres );
	if ( g_iR != CL_SUCCESS ) {
		ShowMessage ( "Application", "Error in setting of kernel argument kNumSpheres." );
		std::cout << "g_iR " << g_iR << " .\n";

	}

	size_t     size_tGlobalWorkSize = kImageWidth / 2 * kImageHeight / 2;
	g_iR                                   = clEnqueueNDRangeKernel ( g_CLCommandQueue, g_CLKernel_4RaysPacketTracing, 1, NULL, & size_tGlobalWorkSize, NULL, 0, NULL, NULL );
	if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in g_CLKernel_4RaysPacketTracing execution." );*/



	// //---4-rays packet raytracing with BVH Tree.-------------------------------------
	// g_iR                                     = clSetKernelArg ( g_CLKernel_4RaysPacketTracing_WithBVHTree, 0, sizeof ( cl_mem ), & cl_mem_Spheres );
	// if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument cl_mem_Spheres." );

	// g_iR                                     = clSetKernelArg ( g_CLKernel_4RaysPacketTracing_WithBVHTree, 1, sizeof ( cl_mem ), & cl_mem_BVHTree );
	// if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument cl_mem_BVHTree." );

	// g_iR                                     = clSetKernelArg ( g_CLKernel_4RaysPacketTracing_WithBVHTree, 2, sizeof ( cl_mem ), & cl_mem_ImageResult );
	// if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument cl_mem_ImageResult." );

	// g_iR                                     = clSetKernelArg ( g_CLKernel_4RaysPacketTracing_WithBVHTree, 3, sizeof ( cl_uint ), & kImageWidth );
	// if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument kImageWidth." );
	// g_iR                                     = clSetKernelArg ( g_CLKernel_4RaysPacketTracing_WithBVHTree, 4, sizeof ( cl_uint ), & kImageHeight );
	// if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in setting of kernel argument kImageHeight." );

	// /*g_iR                                     = clSetKernelArg ( g_CLKernel_4RaysPacketTracing_WithBVHTree, 5, sizeof ( cl_uint ), & kNumSpheres );
	// if ( g_iR != CL_SUCCESS ) {
	// 	ShowMessage ( "Application", "Error in setting of kernel argument kNumSpheres." );
	// 	std::cout << "g_iR " << g_iR << " .\n";

	// }*/

	// /*There is no need to pass cl_uint_BVHNodesListSize to kernel, because we will traverse tree by indices of child nodes and will complete traversing of
	// branches, when there 'll be no nodes to traverse left.

	// cl_uint cl_uint_BVHNodesListSize = g_std_vector_BVHNodesList.size ()
	// g_iR                                     = clSetKernelArg ( g_CLKernel_4RaysPacketTracing_WithBVHTree, 4, sizeof ( cl_uint ), & cl_uint_BVHNodesListSize );
	// if ( g_iR != CL_SUCCESS ) {
	// 	ShowMessage ( "Application", "Error in setting of kernel argument cl_uint_BVHNodesListSize." );
	// 	std::cout << "g_iR " << g_iR << " .\n";

	// }*/

	// size_t     size_tGlobalWorkSize = kImageWidth / 2 * kImageHeight / 2;
	// g_iR                                   = clEnqueueNDRangeKernel ( g_CLCommandQueue, g_CLKernel_4RaysPacketTracing_WithBVHTree, 1, NULL, & size_tGlobalWorkSize, NULL, 0, NULL, NULL );
	// if ( g_iR != CL_SUCCESS ) ShowMessage ( "Application", "Error in g_CLKernel_4RaysPacketTracing_WithBVHTree execution." );



	//clEnqueueReadBuffer ( g_CLCommandQueue, cl_mem_ShortestPathsWeightsMatrix, CL_TRUE, 0, sizeof ( float ) * AMOUNT_OF_NODES * AMOUNT_OF_NODES, ShortestPathsWeights, 0, NULL, NULL );
	//clEnqueueReadBuffer ( g_CLCommandQueue, cl_mem_NextElementsMatrix, CL_TRUE, 0, sizeof ( int ) * AMOUNT_OF_NODES * AMOUNT_OF_NODES, MatrixOfNextElements, 0, NULL, NULL );

	clEnqueueReadBuffer ( m_CLCommandQueue, cl_mem_Vertices, CL_TRUE, 0, sizeof ( float ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS * I_AMOUNT_OF_VERTICES_ON_Y_AXIS * 3, & m_aVertices [ 0 ], 0, NULL, NULL );



	//---Calculating tangents and normals.---------------------------------------------
	iR                                     = clSetKernelArg ( m_CLKernel_ComputeTangentsAndNormals, 0, sizeof ( cl_mem ), & cl_mem_Vertices );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in setting of m_CLKernel_ComputeTangentsAndNormals argument cl_mem_Vertices." ), * GetName () );
	iR                                     = clSetKernelArg ( m_CLKernel_ComputeTangentsAndNormals, 1, sizeof ( cl_mem ), & cl_mem_Normals );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in setting of m_CLKernel_ComputeTangentsAndNormals argument cl_mem_Normals." ), * GetName () );
	iR                                     = clSetKernelArg ( m_CLKernel_ComputeTangentsAndNormals, 2, sizeof ( cl_mem ), & cl_mem_Tangents );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in setting of m_CLKernel_ComputeTangentsAndNormals argument cl_mem_Tangents." ), * GetName () );

	iR                                     = clSetKernelArg ( m_CLKernel_ComputeTangentsAndNormals, 3, sizeof ( cl_uint ), & I_AMOUNT_OF_VERTICES_ON_X_AXIS );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in setting of m_CLKernel_ComputeTangentsAndNormals argument I_AMOUNT_OF_VERTICES_ON_X_AXIS." ), * GetName () );
	iR                                     = clSetKernelArg ( m_CLKernel_ComputeTangentsAndNormals, 4, sizeof ( cl_uint ), & I_AMOUNT_OF_VERTICES_ON_Y_AXIS );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in setting of m_CLKernel_ComputeTangentsAndNormals argument I_AMOUNT_OF_VERTICES_ON_Y_AXIS." ), * GetName () );

	size_tGlobalWorkSize = I_AMOUNT_OF_VERTICES_ON_X_AXIS * I_AMOUNT_OF_VERTICES_ON_Y_AXIS;
	iR                                   = clEnqueueNDRangeKernel ( m_CLCommandQueue, m_CLKernel_ComputeTangentsAndNormals, 1, NULL, & size_tGlobalWorkSize, NULL, 0, NULL, NULL );
	if ( iR != CL_SUCCESS )
		UE_LOG ( LogTemp, Error, TEXT ( "AWaterPlane.InitializeOpenCL () : %s : Error in m_CLKernel_ComputeTangentsAndNormals execution." ), * GetName () );

	clEnqueueReadBuffer ( m_CLCommandQueue, cl_mem_Tangents, CL_TRUE, 0, sizeof ( float ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS * I_AMOUNT_OF_VERTICES_ON_Y_AXIS * 3, & m_aTangents [ 0 ], 0, NULL, NULL );
	clEnqueueReadBuffer ( m_CLCommandQueue, cl_mem_Normals, CL_TRUE, 0, sizeof ( float ) * I_AMOUNT_OF_VERTICES_ON_X_AXIS * I_AMOUNT_OF_VERTICES_ON_Y_AXIS * 3, & m_aNormals [ 0 ], 0, NULL, NULL );

}
